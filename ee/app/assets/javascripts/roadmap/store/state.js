export default () => ({
  epics: [],
  epicIds: [],
  currentGroupId: -1,
  timeframe: [],
  presetType: '',
  sortedBy: '',
});
